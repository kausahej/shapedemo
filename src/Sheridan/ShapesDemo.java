package sheridan;

import java.lang.UnsupportedOperationException;


public class ShapesDemo {

	public static void calculateArea( Rectangle r ) {
		r.setWidth( 2 );
		r.setHeight( 3 );
		
		assertEquals( "Area calculation is incorrect", r.getArea( ), 6 );
	}
	
	public static void main(String[] args) {
		
		ShapesDemo.calculateArea( new Rectangle( ) );
		
		
		ShapesDemo.calculateArea( new Square( ) );
	}

    private static void assertEquals(String area_calculation_is_incorrect, int area, int i) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }
}
