package sheridan;

public class Square extends Rectangle {

        @Override
	public int getArea() {
		return getWidth() * getWidth(); //height and width are the same for square
	}
	
        @Override
	public int getPerimeter() {
		return 4 * getWidth(); //height and width are the same for square
	}
	
}
