package sheridan;

public class Rectangle {

	private int height;
	private int width;
	
	public int getArea() {
		return height * width;
	}
	
	public int getPerimeter() {
		return 2 * (height + width);
	}
	
	public int getHeight() {
		return height;
	}
	
	public int getWidth() {
		return width;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
	
	public void setWidth(int width) {
		this.width = width;
	}
	
}
