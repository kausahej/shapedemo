package Week10;

import Week10.CalculatorOperation;

public class Addition implements CalculatorOperation {
    private double left;
    private double right;
    private double result;


    @Override
    public void perform() {
        result = left + right;
    }
}